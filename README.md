Docker utilizando um composer

Criador: Bruno Cavalcanti

Segue aplicações configuradas

1 - MongoDB

	Esse container possui volume criado no docker
	A imagem utilizada foi a original
	Sem mais alterações
	Link criado para mongo

2 - Elasticsearch
	
	Esse container possui mapeamento com volume criado em docker
	Sem mais lterações
	Link criado elasticsearch

3 - Graylog

	Esse container possui secret que corresponde a senha admin
	Contem mapeamento de volume
	Link graylog

4 - Nginx
	
	Esse container possui mapeamento para o diretorio de execução do docker-compose
	variavel $PWD
	arquivo de configuração graylog.conf
	Sem mais alterações

